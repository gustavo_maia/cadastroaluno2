
package javafxapplication1;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class GerenciadorAluno {
    private final static ObservableList<Aluno> alunos = FXCollections.observableArrayList(); 
    
    public void salvar(Aluno aluno) {
	alunos.add(aluno);
    }

    public ObservableList<Aluno> getAlunos() {
	return alunos;
    }
    
}
