/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javafxapplication1;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBoxBuilder;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author gustavo_maia
 */
public class JavaFXApplication1 extends Application {
    
    @Override
    public void start(Stage stage) {
        
        GridPane grid = new GridPane();
        grid.setVgap(10);
        grid.setHgap(10);
        
        Text texto = new Text("Cadastro de Alunos");
        texto.setFont(Font.font("Verdana", 20));
        grid.add(texto, 0, 0, 2, 1);
        
        //NOME
        Label nomeL = new Label("Nome: ");
        final TextField nomeF = new TextField();
        grid.add(nomeL, 0, 1);
        grid.add(nomeF, 1, 1);
        
        //RA
        Label raL = new Label("RA: ");
        final TextField raF = new TextField();
        grid.add(raL, 0, 2);
        grid.add(raF, 1, 2);
        
        
        //SEXO
        Label sexoL = new Label("Sexo: ");
        grid.add(sexoL, 0, 3);
        
        HBox caixaSexo = new HBox();
        grid.add(caixaSexo, 1, 3);
        
        final ToggleGroup tg = new ToggleGroup();
        final RadioButton rbMasc = new RadioButton("Masc ");
        rbMasc.setToggleGroup(tg);
        rbMasc.setSelected(true);
        final RadioButton rbFemin = new RadioButton("Femin");
        rbFemin.setToggleGroup(tg);
        
        tg.selectedToggleProperty().addListener(new ChangeListener<Toggle>(){

            @Override
            public void changed(ObservableValue<? extends Toggle> ov, Toggle t, Toggle t1) {
                
            }
            
        });
        caixaSexo.getChildren().addAll(rbMasc, rbFemin);
        
        //ESTADO
        Label estado = new Label("Estado: ");
        grid.add(estado, 0, 4);
        
        final ComboBox cb = new ComboBox();
        cb.getItems().addAll(
                "MS",
                "SP",
                "RJ"
        );
        grid.add(cb, 1, 4);
        
        //BOTÕES
        HBox caixaBotao = new HBox();
        grid.add(caixaBotao, 1, 5);
        caixaBotao.setAlignment(Pos.BOTTOM_RIGHT);
        caixaBotao.setSpacing(10);
        Button salvar = new Button("Salvar");
        Button limpar = new Button("Limpar");
        Button listar = new Button("Listar");
        caixaBotao.getChildren().addAll(salvar, limpar, listar);
        
        salvar.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
                Aluno aluno = new Aluno();
                aluno.setNome(nomeF.getText());
                aluno.setRa(raF.getText());
                if(tg.getSelectedToggle() == rbMasc){
                    aluno.setSexo("Masculino");
                }else if(tg.getSelectedToggle() == rbFemin){
                    aluno.setSexo("Feminino");
                }else{
                    aluno.setSexo("Não informado");
                }
                aluno.setEstado(cb.getValue().toString());
                aluno.setCb(new CheckBox());
                
                GerenciadorAluno ga = new GerenciadorAluno();
                ga.salvar(aluno);
                
                Button b = new Button("OK");
                b.setMaxHeight(15);
                final Stage dialogStage = new Stage();
                dialogStage.initModality(Modality.WINDOW_MODAL);
                dialogStage.setScene(new Scene(VBoxBuilder.create().
                    children(new Text("Aluno Cadastrado com Sucesso"), b).
                    alignment(Pos.CENTER).padding(new Insets(5)).build()));
                dialogStage.show();
                b.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent t) {
                        dialogStage.close();
                    }
                });
            }
        });
        
        listar.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
                Stage secondStage = new Stage();
                Layout layout = new Layout();
                
                Scene cena2 = new Scene(layout, 537, 500);
                secondStage.setScene(cena2);
                secondStage.show();
            }
        });
        
        limpar.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
                raF.setText("");
                nomeF.setText("");
                rbMasc.setSelected(false);
                rbFemin.setSelected(false);
                cb.setValue(null);
            }
        });
        
        
        Scene cena = new Scene(grid, 300, 250);
        
        stage.setTitle("Cadastro");
        stage.setScene(cena);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
    
}
